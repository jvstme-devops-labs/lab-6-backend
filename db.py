from datetime import datetime
from typing import Annotated

from fastapi import Depends
from sqlalchemy import TIMESTAMP, create_engine
from sqlalchemy.orm import DeclarativeBase, Mapped, Session, mapped_column

from settings import settings


engine = create_engine(settings.db_url)


def create_schema():
    Base.metadata.create_all(engine)


def get_db():
    db = Session(engine)
    try:
        yield db
    finally:
        db.close()


DB = Annotated[Session, Depends(get_db)]


class Base(DeclarativeBase):
    pass


class Increment(Base):
    __tablename__ = "increments"

    id: Mapped[int] = mapped_column(primary_key=True)
    date: Mapped[datetime] = mapped_column(TIMESTAMP)
